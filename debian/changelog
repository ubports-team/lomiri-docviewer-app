lomiri-docviewer-app (3.1.1+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 03 Mar 2025 08:19:24 +0100

lomiri-docviewer-app (3.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version to 4.7.0. No changes needed.
  * debian/patches:
    + Drop patches 0001, 0002, 0003, and 0004. All applied upstream.
    + Drop 2001_no-unit-tests.patch. Tests can be disabled via CMake option now.
  * debian/rules:
    + Disable unit tests.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 11 Sep 2024 21:25:23 +0200

lomiri-docviewer-app (3.0.4+dfsg-3) unstable; urgency=medium

  [ Mike Gabriel ]
  * debian/watch:
    + Update file for recent GitLab changes of the tags overview page.

  [ Guido Berhoerster ]
  * debian/patches:
    + Add patch 0004 from upstream in order to fix i18n.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 30 Apr 2024 12:28:28 +0200

lomiri-docviewer-app (3.0.4+dfsg-2) unstable; urgency=medium

  * debian/patches:
    + Add patch files 0001, 0002 and 0003. Fix missing desktop icon and missing
      splash screen.
  * debian/control:
    + Switch from pkg-config to pkgconf. Thanks, lintian.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 26 Feb 2024 10:09:01 +0100

lomiri-docviewer-app (3.0.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update copyright attribution for debian/.
    + Update copyright attributions.
  * debian/rules:
    + Don'ship lomiri-docviewer-app-migrate.py in Debian.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 02 Feb 2024 23:37:07 +0100

lomiri-docviewer-app (3.0.3+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - Translation updates.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 27 Feb 2023 14:57:09 +0100

lomiri-docviewer-app (3.0.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/watch:
    + Start watching upstream releases.
  * debian/patches:
    + Drop patches 1001, 1002, 1003 and 2000. All applied upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 04 Feb 2023 21:13:39 +0100

lomiri-docviewer-app (3.0.1+git20230127.ab13dca+dfsg-1) unstable; urgency=medium

  * Initial upload to Debian. (Closes: #1030026).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 30 Jan 2023 15:31:03 +0100
